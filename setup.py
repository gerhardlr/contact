#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

requirements = [
    # TODO: put package requirements here
]

test_requirements = [
    'assertpy',
    'pytest'
]


setup(
    name='contact',
    version='0.1.0',
    description="Generates a list of contacts (two sensors being at the same location at the same time) over a specific time window with O(n)  performance",
    author="Gerhard le Roux",
    author_email='gerhardlr@gmail.com',
    url='https://gitlab.com/gerhardlr/contact',
    packages=[
        'contact',
    ],
    package_dir={'contact':
                 'contact'},

    include_package_data=True,
    install_requires=requirements,
    license="BSD license",
    zip_safe=False,
    keywords='contact',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ], 
    test_suite='tests',
    tests_require=test_requirements

)
