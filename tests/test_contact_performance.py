#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
from assertpy import assert_that
import logging
import time

from tests.helpers import measurements_circle,build_aggregate_measurements,agents_from_circular_tranform,build_aggregate_contacts, get_size

#the unit under test is contact
from contact.contact import get_contacts

logger = logging.getLogger(__name__)

def test_getcontacts_performace():
    #given a set of 3 clusters (circular)
    template = measurements_circle
    nr_of_times = 130000
    offset = 40
    measurements = build_aggregate_measurements(measurements_circle,offset,nr_of_times )
    measurements_size = get_size(measurements)/(1024*1024)
    #when
    logger.info("starting job for measurement set = {:0.3f}MB".format(measurements_size))
    t = time.perf_counter()
    result = get_contacts(measurements)
    elapsed_time = time.perf_counter() - t
    result_size = get_size(result)/(1024*1024)
    nr_of_records = nr_of_times*8
    throughput = nr_of_records/elapsed_time
    logger.info("job finished at {!s} seconds for {!s} records (throughput = {:d}) (size: {:0.3f}MB)".format(elapsed_time,nr_of_records,int(throughput),result_size))
    #then
    template = agents_from_circular_tranform
    expected_result = build_aggregate_contacts(template,nr_of_times)
    threshold_throughput = 10000
    failed_throughput = 1000
    if (throughput < threshold_throughput):
        logger.warn('througput is below threshold of 10000 ({:d)'.format(throughput))
    assert_that(result).is_equal_to(expected_result)
    assert_that(throughput).is_greater_than(failed_throughput)

