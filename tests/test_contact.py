#!/usr/bin/env python
# -*- coding: utf-8 -*-


import pytest
from assertpy import assert_that
from unittest.mock import patch
from tests.helpers import result_from_transform_12_18,measurements_12_18_times_3,measurements_circle,indexes_from_measure_circle,\
    indexes_from_measurements_12_18_times_3, agents_from_circular_tranform,\
    build_aggregate_contacts,build_aggregate_measurements

#the unit under test is contact
from contact.contact import  _transform, _set_indexes, _get_contacts_for_agents, get_contacts,_update_agents,update_indexes_with_primary,_update_indexes_with_adjacency



def test_tranform():
    #given
    x =  '12'
    y = '18'
    ##when
    result_from_transform = _transform(x,y)
    ##then
    assert_that(result_from_transform).is_equal_to(result_from_transform_12_18)

def test_update_agents_when_empty():
    #given an empty set of agents
    agents = {}
    #and a new agent 'a1'
    agent = 'a1'
    #and a contact 'a2'
    contact = 'a2'
    #when
    _update_agents(agents,agent,contact)
    #then
    expected_result = {\
        'a1':{\
            'contacts':[\
                'a2'\
            ]\
        }\
    }
    assert_that(agents).is_equal_to(expected_result)

def test_update_agents_when_existing():
    #given an existing set of agents
    agents = {\
        'a1':{'contacts':['a2']}\
    }
    #and a new agent 'a1'
    agent = 'a1'
    #and a contact 'a2'
    contact = 'a3'
    #when
    _update_agents(agents,agent,contact)
    #then
    expected_result = {\
        'a1':{'contacts':['a2','a3']}\
    }
    assert_that(agents).is_equal_to(expected_result)

def test_update_indexes_with_primary_when_empty():
    #given
    indexes = {}
    block = '10_15'
    item_id = 'a1'
    #when
    update_indexes_with_primary(indexes,block,item_id)
    #then
    expected_result = {\
        '10_15': {'prim':['a1'],'adj':[]}
        }
    assert_that(indexes).is_equal_to(expected_result)

def test_update_indexes_with_primary_when_existing():
    #given
    indexes = {\
        '10_15': {'prim':['a1'],'adj':[]}
        }
    block = '10_15'
    item_id = 'a2'
    #when
    update_indexes_with_primary(indexes,block,item_id)
    #then
    expected_result = {\
        '10_15': {'prim':['a1','a2'],'adj':[]}
        }
    assert_that(indexes).is_equal_to(expected_result)

def test_update_indexes_with_adj_when_empty():
    #given
    indexes = {}
    block = '10_15'
    item_id = 'a1'
    #when
    _update_indexes_with_adjacency(indexes,block,item_id)
    #then
    expected_result = {\
        '10_15': {'prim':[],'adj':['a1']}
        }
    assert_that(indexes).is_equal_to(expected_result)

def test_update_indexes_with_adj_when_existing():
    #given
    indexes = {\
        '10_15': {'prim':[],'adj':['a1']}
        }
    block = '10_15'
    item_id = 'a2'
    #when
    _update_indexes_with_adjacency(indexes,block,item_id)
    #then
    expected_result = {\
        '10_15': {'prim':[],'adj':['a1','a2']}
        }
    assert_that(indexes).is_equal_to(expected_result)


@patch("contact.contact._transform")
def test_set_indexes_12_and_8_times_3(mock_transform):
    #given a particular transformation for input ( x= 12 and y =18)
    mock_transform.return_value = result_from_transform_12_18
    #given a particular measurement set for three measurements at the same location ( x= 12 and y =18)
    measurements = measurements_12_18_times_3
    #when
    result_from_set_nr1 = _set_indexes(measurements)
    #then
    #check that it behaved correctly
    assert_that(mock_transform.call_count).is_equal_to(3)
    #check that it calculated correctly
    assert_that(result_from_set_nr1).is_equal_to(indexes_from_measurements_12_18_times_3)

@patch("contact.contact._transform")
def test_set_indexes_12_and_8_times_8(mock_transform):
    #given a particular transformation for input ( x= 12 and y =18)
    mock_transform.return_value = result_from_transform_12_18
    #given a particular measurement set for 6 measurements at the same location ( x= 12 and y =18) 
    measurements = measurements_circle
    #note the actual locations is arbitrary because we have moved the result to fixed - the  id nrs will however have changed
    #when
    result_from_set = _set_indexes(measurements)
    #then
    #check that it behaved correctly
    assert_that(mock_transform.call_count).is_equal_to(8)
    #check that it calculated correctly
    assert_that(result_from_set['10_15']['prim']).is_length(8)
    assert_that(result_from_set['10_15']['adj']).is_length(0)
    assert_that(result_from_set['5_15']['prim']).is_length(0)
    assert_that(result_from_set['5_15']['adj']).is_length(8)

def test_set_with_transform():
    #given a circular (symetrical) set of measurements
    measurements = measurements_circle
    #when
    #calculate using both the internal _transform and _set_indexes
    result_from_set = _set_indexes(measurements)
    #then
    assert_that(result_from_set).is_length(len(indexes_from_measure_circle))
    assert_that(result_from_set).is_equal_to(indexes_from_measure_circle)
    #assert_that(result_from_set).is_subset_of(indexes_from_measure_circle)

def test_update_agents():
    #given a list of indexes for positions obtained from measure circle
    indexes  = indexes_from_measure_circle
    #when
    result = _get_contacts_for_agents(indexes)
    #then
    assert_that(result).is_equal_to(agents_from_circular_tranform)

###integration tests

def test_get_contacts_from_circle():
    #given a circular (symetrical) set of measurements
    measurements = measurements_circle
    #when
    result = get_contacts(measurements)
    #then
    assert_that(result).is_equal_to(agents_from_circular_tranform)

