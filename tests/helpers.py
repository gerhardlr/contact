import sys

result_from_transform_12_18 = [\
#1st one is primary
'10_15',\
#2nd one must be adjecent to the left (e.g. x - 5)
'5_15',\
#3d one must be adjecent to the top left (e.g. x - 5 and y + 5)
'5_20',\
#4th one must be adjecent to the bottom left (e.g. x - 5 and y - 5)
'10_20',\
#5th one must be a adjecent to the right (e.g. x + 5 and y)
'15_20',\
#6th one must be a adjecent to the top right (e.g. x + 5 and y + 5)
'15_15',\
#7th one must be a adjecent to the bottom right (e.g. x + 5 and y-5)
'15_10',\
#8th one must be a adjecent directly above (e.g. x and y+5)
'10_10',\
#9th one must be a adjecent directly from below (e.g. x and y-5)
'5_10'\
]

measurements_12_18_times_3 = [\
    {'source_id' : 'a1','position':{'x':'12','y':'18'}},\
    {'source_id' : 'a2','position':{'x':'12','y':'18'}},\
    {'source_id' : 'a3','position':{'x':'12','y':'18'}}\
]


measurements_circle = [\
    {'source_id' : 'a1','position':{'x':'7','y':'17'}},\
    {'source_id' : 'a2','position':{'x':'12','y':'22'}},\
    {'source_id' : 'a3','position':{'x':'17','y':'27'}},\
    {'source_id' : 'a4','position':{'x':'22','y':'22'}},\
    {'source_id' : 'a5','position':{'x':'27','y':'17'}},\
    {'source_id' : 'a6','position':{'x':'22','y':'12'}},\
    {'source_id' : 'a7','position':{'x':'17','y':'7'}},\
    {'source_id' : 'a8','position':{'x':'12','y':'12'}}\
]

indexes_from_measure_circle= {\
'5_15': {'prim':['a1',],'adj':['a2','a8']},\
'0_15': {'prim':[],'adj':['a1']},\
'0_20': {'prim':[],'adj':['a1']},\
'5_20':{'prim':[],'adj':['a1','a2']},\
'10_20':{'prim':['a2'],'adj':['a1','a3']},\
'10_15':{'prim':[],'adj':['a1','a2','a8']},\
'10_10':{'prim':['a8'],'adj':['a1','a7']},\
'5_10':{'prim':[],'adj':['a1','a8']},\
'0_10':{'prim':[],'adj':['a1']},\
#a2
'5_25':{'prim':[],'adj':['a2']},\
'10_25':{'prim':[],'adj':['a2','a3']},\
'15_25':{'prim':['a3'],'adj':['a2','a4']},\
'15_20':{'prim':[],'adj':['a2','a3','a4']},\
'15_15':{'prim':[],'adj':['a2','a4','a6','a8']},\
#a3
'10_30':{'prim':[],'adj':['a3']},\
'15_30':{'prim':[],'adj':['a3']},\
'20_30':{'prim':[],'adj':['a3']},\
'20_25':{'prim':[],'adj':['a3','a4']},\
'20_20':{'prim':['a4'],'adj':['a3','a5']},\
#a4
'25_25':{'prim':[],'adj':['a4']},\
'25_20':{'prim':[],'adj':['a4','a5']},\
'25_15':{'prim':['a5'],'adj':['a4','a6']},\
'20_15':{'prim':[],'adj':['a4','a5','a6']},\
#a5
'30_20':{'prim':[],'adj':['a5']},\
'30_15':{'prim':[],'adj':['a5']},\
'30_10':{'prim':[],'adj':['a5']},\
'25_10':{'prim':[],'adj':['a5','a6']},\
'20_10':{'prim':['a6'],'adj':['a5','a7']},\
#a6
'15_10':{'prim':[],'adj':['a6','a7','a8']},\
'25_5':{'prim':[],'adj':['a6']},\
'20_5':{'prim':[],'adj':['a6','a7']},\
'15_5':{'prim':['a7'],'adj':['a6','a8']},\
#a7
'10_5':{'prim':[],'adj':['a7','a8']},\
'20_0':{'prim':[],'adj':['a7']},\
'15_0':{'prim':[],'adj':['a7']},\
'10_0':{'prim':[],'adj':['a7']},\
#a8
'5_5':{'prim':[],'adj':['a8']}\
}

indexes_from_measurements_12_18_times_3 = {\
#primary index 
    '10_15': {'prim':['a1','a2','a3'],'adj':[]},\
#2nd one must be adjecent to the left (e.g. x - 5)
    '5_15': {'prim':[],'adj':['a1','a2','a3']},\
#3d one must be adjecent to the top left (e.g. x - 5 and y + 5)
    '5_20': {'prim':[],'adj':['a1','a2','a3']},\
#4th one must be adjecent to the bottom left (e.g. x - 5 and y - 5)
    '5_10':{'prim':[],'adj':['a1','a2','a3']},\
#5th one must be a adjecent to the right (e.g. x + 5 and y)
    '15_15':{'prim':[],'adj':['a1','a2','a3']},\
#6th one must be a adjecent to the top right (e.g. x + 5 and y + 5)
    '15_20':{'prim':[],'adj':['a1','a2','a3']},\
#7th one must be a adjecent to the bottom right (e.g. x + 5 and y-5)
    '15_10':{'prim':[],'adj':['a1','a2','a3']},\
#8th one must be a adjecent directly above (e.g. x and y+5)
    '10_20':{'prim':[],'adj':['a1','a2','a3']},\
#9th one must be a adjecent directly from below (e.g. x and y-5)
    '10_10':{'prim':[],'adj':['a1','a2','a3']}\
}

agents_from_circular_tranform = {\
'a1': {'contacts': ['a2', 'a8']},\
'a2': {'contacts': ['a1', 'a3']},\
'a8': {'contacts': ['a1', 'a7']},\
'a3': {'contacts': ['a2', 'a4']},\
'a4': {'contacts': ['a3', 'a5']},\
'a5': {'contacts': ['a4', 'a6']},\
'a6': {'contacts': ['a5', 'a7']},\
'a7': {'contacts': ['a6', 'a8']}\
}

def build_agents_from_template(template,cluster_id):
    return {\
    '{}_{}'.format(cluster_id,key):{\
        'contacts':[\
            '{}_{}'.format(cluster_id,x)\
                 for x in template[key]['contacts']\
        ]}\
    for key in template}

def offset_cluster(cluster,offsetx,cluster_id,offsety=0,):
    newcluster = [{\
        'source_id':'{!s}_{!s}'.format(cluster_id,x['source_id']),\
        'position':{\
            'x':'{!s}'.format(int(x['position']['x'])+offsetx),\
            'y':'{!s}'.format(int(x['position']['y'])+offsety)\
        }}\
     for x in measurements_circle\
    ]
    return(newcluster)

def build_aggregate_measurements(template_cluster,offset,nr_of_times):
    build = []
    aggregate_offset = 0
    for i in range(0,nr_of_times):
        cluster_id = 'cl{!s}'.format(i)
        new_cluster = offset_cluster(template_cluster,aggregate_offset,cluster_id)
        build += new_cluster
        aggregate_offset += offset
    return build

def build_aggregate_contacts(template,nr_of_times):
    build = {}
    for i in range(0,nr_of_times):
        cluster_id = 'cl{!s}'.format(i)
        new_build = build_agents_from_template(template,cluster_id)
        build.update(new_build)
    return build

def get_size(obj, seen=None):
    """Recursively finds size of objects"""
    size = sys.getsizeof(obj)
    if seen is None:
        seen = set()
    obj_id = id(obj)
    if obj_id in seen:
        return 0
    # Important mark as seen *before* entering recursion to gracefully handle
    # self-referential objects
    seen.add(obj_id)
    if isinstance(obj, dict):
        size += sum([get_size(v, seen) for v in obj.values()])
        size += sum([get_size(k, seen) for k in obj.keys()])
    elif hasattr(obj, '__dict__'):
        size += get_size(obj.__dict__, seen)
    elif hasattr(obj, '__iter__') and not isinstance(obj, (str, bytes, bytearray)):
        size += sum([get_size(i, seen) for i in obj])
    return size