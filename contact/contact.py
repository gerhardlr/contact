# -*- coding: utf-8 -*-

_M = 5


###to be factored out as a data access layer


#####

def _transform(x,y):
    '''
    transforms a two dimensional cooridnate(x,y) into a set of index blocks dimensioned by the _M parameter (default = 5)
    these index blocks comprise of the the primary one (the one in which it resides) and all  8 the adjacent ones bordering it
    e.g. four apposites and 4 corners
    the primary block is the first item in the list followed by [left, top left corner, bottom right corner, right, top right corner, bottom right corner, top, bottom]
    '''
    #converts the numbers into multiples of _M in downward direction
    x = int(x)
    y =int(y)
    x = x -(x % _M)
    y = y - (y % _M)
    #creates a set of primary and adjacent indexes (coordinates) based on multiples of _M
    return ['{!s}_{!s}'.format(x,y),\
        '{!s}_{!s}'.format(x-_M,y),\
        '{!s}_{!s}'.format(x-_M,y+_M),\
        '{!s}_{!s}'.format(x,y+_M),\
        '{!s}_{!s}'.format(x+_M,y+_M),\
        '{!s}_{!s}'.format(x+_M,y),\
        '{!s}_{!s}'.format(x+_M,y-_M),\
        '{!s}_{!s}'.format(x,y-_M),\
        '{!s}_{!s}'.format(x-_M,y-_M)]
  
def update_indexes(indexes,block,type_of_update,item_id):
    ''' update an index and create a new one if it alread exists
    '''
    if type_of_update == 'primary':
        list_type = 'prim'
        new_entry = {'prim':[item_id],'adj':[]}
    elif type_of_update == 'adjacency':
        list_type = 'adj'
        new_entry = {'prim':[],'adj':[item_id]}
    #if it exists already append the itemd_id
    if block in indexes: 
        indexes[block][list_type].append(item_id) 
    #if not append a new entry as determined by the list type
    else: 
        indexes[block] = new_entry

def update_indexes_with_primary(indexes,block,item_id):
    update_indexes(indexes,block,'primary',item_id)

def _update_indexes_with_adjacency(indexes,block,item_id):
    update_indexes(indexes,block,'adjacency',item_id)

def _set_indexes(measurements):
    '''
    Given a list of measurements (source_id,position) derive a list of indexes blocks containing these measurements.
    Index blocks are dimensioned by a predefined distance (default 5) and contains two lists:
    1. **Primary list:** a list of all the measurements located inside the index block
    2. **Adjacency List:** a list of all the measurements adjancent to that index block
    '''
    indexlist = {}
    #traverse through each item of a measurement, get a set neigboring and and primary indexes to which the measurement belongs based on location and updates those indexes with this id
    for item in measurements:
        #get the list of blocks (adjacent as well as actual) for particular measurement
        blocks = _transform(item['position']['x'],item['position']['y'])
        item_id = item['source_id']
        # as above the primary block is defined to be the first one
        prim_block = blocks[0]
        update_indexes_with_primary(indexlist,prim_block,item_id)
        for block in blocks[1:]:
            _update_indexes_with_adjacency(indexlist,block,item_id)

    return indexlist

    

def _update_agents(agents,agent,contact):
    '''
    Update a list of agents. Create a new entry if it doesnt exist.
    '''
    if agent in agents:
        agents[agent]['contacts'].append(contact)
    else:
        agents[agent] = {'contacts':[contact]}

def _get_contacts_for_agents(indexlist):
    '''
    by running through the list of indexes (boxes) created, generate a contact as the combination 
    of primary agents with other primary agents and primary agents with adjacent agents located in that index
    **paramaters**
    indexlist = dictionary object (has table) located by an index name

    '''
    agents = {}
    for index in indexlist:
        primaries = indexlist[index]['prim']
        adjacencies = indexlist[index]['adj']
        for primary_agent in primaries:
            for contact in adjacencies:
                _update_agents(agents,primary_agent,contact)
            for other_primary in primaries:
                if other_primary != primary_agent:
                    contact = other_primary
                    _update_agents(agents,primary_agent,contact)
    return agents

def get_contacts(measurement_set):
    '''
    return a list of agents with contacts that has provided a location via a measurement set for a period of time. GA contact is defined
    when two agents are within a certain range from each other (set by _M). The algorithm calculate this in O(n) time through the creation of indexes,
    each containing (1) a list of real agents and (2) a list of agents adjacent to it. The contacts can then be harvested by going through all the created
    indexes and generating a contact for each combination of real agents with other real agents as well as real agents with adjacent ones.
    '''
    indexes = _set_indexes(measurement_set)
    return _get_contacts_for_agents(indexes)