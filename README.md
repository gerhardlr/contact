### Contact

Generates a list of contacts (two agents being at the same location at the same time) over a specific time window with O(n) performance.

### How it works

Given a large set of records,each denoting an agent being at a specific location, module will generate a list of other contacts that were in the proximity of that contact for each individual record.

### Schema
This module does not come with a module for interacting with your database but assumes the data is loaded in a list of dictionaries with the following schema for each agent in the list:

***source_id:*** string identifying the agent uniquely

***position:*** an object containing the two dimensional position as follows:
    
***x:*** the lat position converted into meters (according to your own preference)

***y:*** the long position converted into meters (according to your own preference)

The resulting schema is a dictionary with with the key set on the agent's ***source_id***. The value is an object with ***contacts*** specifying an array of  contacts generated

### Usage

To use this module send the following command: 

```output = get_size(input)```

The module calculates a contact when two agents are within 5 meters from each other. This can however be changes through the global `_M` variable

The throughput of this function is roughly `30 000` records per second. Memory size problems may start creeping up with records larger than '100 000' leading to a typical latency of three seconds.

With measurements larger than `100 000` suggests to index them into seperate areas and perform contacts for each area.

### install

After cloninhg the project you can install either with penv or virtualenv using the corresponding pipfile and requirements file.
